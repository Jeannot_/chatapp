import chatService from '../services/chat.service';

export default chatActions = {
    join,
    sendMessage,
    leave
}

function join(username, room) {
    return dispatch => {
        dispatch(request(username, room));
        chatService.join(username, room,
            messages => dispatch({type: 'SYNC_SUCCESS', messages}),
            error => dispatch({type: 'FAILURE', error})
            )
            .then(messages => dispatch(success(messages)))
            .catch(error => dispatch(failure(error)));
    };
    function request(username, room) { return { type: 'JOIN_REQUEST', username, room } }
    function success(messages) { return { type: 'JOIN_SUCCESS', messages } }
    function failure(error) { return { type: 'FAILURE', error } }
}

function leave(username, room) {
    return dispatch => {
        dispatch({type:'LEAVE_REQUEST', username, room});
        chatService.leave(username, room)
            .then(()=>dispatch({type: 'LEAVE_SUCCESS', username, room}))
            .catch(error => dispatch({ type: 'FAILURE', error }))
    }
}

function sendMessage(message) {
    return dispatch => {
        dispatch({type: 'SEND_MESSAGE_REQUEST', message});
        chatService.sendMessage(message)
            .then(messages => dispatch({type: 'SEND_MESSAGE_SUCCESS', messages}))
            .catch(error => dispatch({ type: 'FAILURE', error}));
    };
}