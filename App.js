import React from 'react';
import { Home, Chat } from './components';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import rootReducer from './reducers';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';

const { Navigator, Screen } = createStackNavigator();
const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export default class App extends React.PureComponent {

  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Navigator
            initialRouteName="Home"
            screenOptions={{
              headerStyle: {
                backgroundColor: '#555',
              },
              headerTintColor: '#fff',
            }}
          >
            <Screen name="Home" component={Home} options={{ title: 'ChatApp' }} />
            <Screen name="Chat" component={Chat} options={({ route: { params } }) => ({ title: `Salon ${params.room}` })} />
          </Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}