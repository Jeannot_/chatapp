const initState = {
    messages: [],
    username: null,
    room: null,
    error: null,
    loading: false
}
export default function (state = initState, action) {
    console.log(action);
    switch (action.type) {
        case 'JOIN_REQUEST':
            return {
                ...state,
                room: action.room,
                user: action.username,
                error: null,
                loading: true
            };
        case 'SEND_MESSAGE_SUCCESS':
        case 'SYNC_SUCCESS':
            return {
                ...state,
                messages: [
                    ...action.messages,
                ]
            };
        case 'JOIN_SUCCESS':
            return {
                ...state,
                messages: action.messages,
                loading: false
            };
        case 'LEAVE_SUCCESS':
            return {
                ...state,
                room: '',
                username: '',
                messages: []
            };
        case 'FAILURE':
            return {
                ...state,
                error: action.error,
                loading: false
            };

        default:
            return state;
    }
}