import React from 'react';
import { View, Text, Button, TextInput, StyleSheet, SafeAreaView, StatusBar, FlatList } from 'react-native';
import moment from 'moment';

export default class Chat extends React.PureComponent {

    listRef = React.createRef();

    state = {
        newMessage: '',
        messages: []
    }

    handleSend = () => {
        const { newMessage, messages } = this.state;
        const { route: { params }} = this.props;
        const { username } = params;
        this.setState({
            messages: [
                ...messages,
                {
                    id: messages.length,
                    author: username,
                    content: newMessage,
                    created_at: new Date()
                }
            ]
        })
    }    

    renderMessage = ( {item: message}) => {
        const { route: { params }} = this.props;
        const { username } = params;

        return(
            <View style={[
                styles.message,
                username == message.author && styles.authorMessage
              ]}>
                <View style={[
                  styles.bubble,
                  username == message.author && styles.authorBubble
                ]}>
                  <Text style={[
                    styles.messageAuthor,
                    username == message.author && { color: '#ddd' }
                  ]}>
                    {message.author}
                  </Text>
                  <Text style={[
                    styles.messageContent,
                    username == message.author && { color: '#ddd' }
                  ]}>
                    {message.content}
                  </Text>
                </View>
                <Text style={styles.timestamp}>
                  {moment(message.created_at).fromNow()}
                </Text>
              </View>
        )
    }

    render() {
      const { newMessage, messages } = this.state;
      const { route: { params }} = this.props;
      const { username, room } = params;

      const Empty = (
          <Text>No messages</Text>
      );
  
      return (
        <SafeAreaView 
            style={{ flex: 1, marginTop: StatusBar.currentHeight || 0 }}
        >
          <FlatList 
            data={messages}
            renderItem={this.renderMessage}
            keyExtractor={item => item.id}
            style={styles.messageList}
            ListEmptyComponent={Empty}
            ref={ref => this.listRef = ref}
            onContentSizeChange={() => this.listRef.scrollToEnd({ animated: true})}
            onLayout={() => this.listRef.scrollToEnd({animated: true})}
          />
          <View style={styles.messageComposer}>
            <TextInput 
                style={styles.textInput}
                onChangeText= {newMessage => this.setState({newMessage})}
                value = {newMessage}
            />
            <Button 
                title="SEND"
                onPress={this.handleSend}
                disabled={newMessage == ''}
            />
          </View>
        </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    messageList: {
      flex: 1,
    },
    message: {
      padding: 8,
      justifyContent: 'flex-start',
      alignItems: 'flex-start'
    },
    authorMessage: {
      alignItems: 'flex-end',
    },
    bubble: {
      padding: 8,
      backgroundColor: '#CCC',
      maxWidth: '80%',
      borderRadius: 16,
      borderBottomLeftRadius: 0
    },
    authorBubble: {
      backgroundColor: '#0000AA',
      borderBottomLeftRadius: 16,
      borderBottomRightRadius: 0
    },
    timestamp: {
      color: '#999999',
      fontSize: 12
    },
    messageAuthor: {
      fontWeight: 'bold',
      color: '#222'
    },
    messageContent: {
      color: '#333'
    },
    messageComposer: {
      flex: 0,
      flexWrap: 'nowrap',
      flexDirection: 'row',
      padding: 4,
      backgroundColor: '#DDDDDD',
      borderTopWidth: 1,
      borderColor: '#AAA',
    },
    textInput: {
      borderWidth: 1,
      borderColor: '#AAA',
      padding: 4,
      flex: 1,
      backgroundColor: '#FFFFFF'
    },
});