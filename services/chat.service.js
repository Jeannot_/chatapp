import PouchDB from 'pouchdb-react-native';

const REMOTE_DB = "http://admin:veryverysecret@gathor.org:8923";

class Chat {

    room = '';
    username = '';
    db = null;
    sync = null;
    messages = [];

    join(username, room, onSync, onFailure) {
        this.username = username || 'Anonymous';
        this.room = room.toLowerCase() || 'default';
        this.room = room
            .toLowerCase()
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            ;

        this.db = new PouchDB(this.room);
        this.sync = this.db.sync(`${REMOTE_DB}/${this.room}`, 
        {
            live: true,
            retry: true,
            continuous: true,
        });
        this.sync.on('change', this.handleChange(onSync));
        this.sync.on('error', this.handleError(onFailure));

        return this.getMessages();
    }

    leave(username, room) {
        chatActions.leave(username, room);
    }

    handleChange = onSync => {
        return async e => {
            console.log(e);
            const { direction, change: { docs: newMessages}} = e;
            //const messages = await this.getMessages();
            /*if(direction == 'push') {
                this.messages = newMessages;
            }*/
            if(direction == 'pull') {
                newMessages.map(message => {
                    if(!this.messages.find(m => m._id == message._id)) {
                        this.messages.push(message);
                    }
                });
                this.messages = this.messages.sort((a, b) => a.created_at > b.created_at ? 1 : -1);
                onSync(this.messages);
            }
        }
    }

    handleError = onFailure => {
        return error => {
            onFailure(error);
        }
    }

    async getMessages() {
        if (!this.db) throw 'No Database;'
        try {
            const response = await this.db.allDocs({ include_docs: true });
            this.messages = response.rows
                .map(r => r.doc).sort((a, b) => a.created_at > b.created_at ? 1 : -1);
            return this.messages;
        } catch (e) {
            console.error(e);
        }
    }

    async sendMessage({ author, content }) {
        if (!this.db) throw 'No Database;'
        try {
            let message = {
                author, content, created_at: new Date()
            }
            const response = await this.db.post(message);
            message =  {
                _id: response.id,
                ...message
            };
            this.messages = [
                ...this.messages,
                message
            ]
            return this.messages;
        }
        catch (e) {
            console.error(e);
        }
    }
}

export default new Chat();